![](github_fontawesome.png)

# Contao Component: Font-Awesome-Free
Stores the Font-Awesome-Free Package in `assets/fontawesome-free`


this cane you use with the follow Contao Extension: [fontawesome-icon-picker-bundle](https://gitlab.com/jedoCodes/fontawesome-icon-picker-bundle)



```


// Import the Font-Awesome-Free component from the assets directory
@import "../../../assets/fontawesome-free/scss/fontawesome.scss";



```






```
to use it in our template use the follow


<link href="../assets/fontawesome-free/scss/fontawesome.scss" rel="stylesheet">


```
